$(function() {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
});
$('.carousel').carousel({
    interval: 1000
});
$('#Especificaciones').on('show.bs.modal', function(e) {
    console.log('Se muestra el modal');
    $('#reseña').removeClass('btn-outline-success')
    $('#reseña').addClass('btn-primary')
    $('#reseña').prop('disable', true)

});
$('#Especificaciones').on('shown.bs.modal', function(e) {
    console.log('el modal se mostro');
});
$('#Especificaciones').on('hide.bs.modal', function(e) {
    console.log('el modal se cerrara');
    $('#reseña').removeClass('btn-primary')
    $('#reseña').addClass('btn-outline-success')

});
$('#Especificaciones').on('hidden.bs.modal', function(e) {
    console.log('el modal se cerro');
    $('#reseña').prop('disable', false)
});