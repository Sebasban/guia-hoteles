﻿git status: visualizo cambios
git add .: adiero cambios
git reset: devuelvo cambios
git commit -m "cadena" : preparo los cambios
git push: subo los cambios
git log: miro quien hizo el commit
touch .gitignore: Muestro la carpeta gitignore, la abro
y coloco la carpeta llamada node_modules, para ingorarla
en el commit

-Pasos para iniciar el proyecto:
npm init
definir nombre
version
descripcion
entry point: punto de inicio donde entraremos a la url
test command:
repositorio git 
keywords
author
licencia
Nos muestra al archivo de configuración

-Definir el servidor web
Lite server: Se lleva muy bien con angular
Usa browsersync
Comando de instalacion 
npm install lite-server --save-dev
edito el package.json
  "scripts": {
    "dev": "lite-server",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
corremos el servidor
npm run dev


Tips de la barra de busqueda
Incluir el logo del sitio
click en el logo redirecciona a la pagina principal
Nombres explicitos de las secciones
pocas secciones
nombres cortos
Color contrastante la seccion donde se encuentra
ubicar el log out claramente

Instalar sass
npm install node-sass --save-dev

Crear el archivo sas:
en package.jsin crear la tarea en scripts
    "scripts": {
        "dev": "lite-server",
        "test": "echo \"Error: no test specified\" && exit 1"
        "scss": "node-sass -o css/ css/"------------> esta
    },
creo una carpeta llamada style.scss y ahi hago las variables que quiera
y lo referencio en el index
ejecutar el archivo .scss para que se vuelva un archivo .css
npm run scss

como instalar less
npm install -g less
ejecuto con 
lessc css/styles2.less css/styles2.css

##########Onchange para automatizar################
Instalar onchange:
npm install --save-dev onchange rimraf
Luego en el package.json añado la instrucción en la seccion de scripts:
"watch:scss": "onchange \"css/*.scss\" -- npm run scss"
ejecutar:
npm run watch:scss


Instalar el concurrently:
npm install --save concurrently
tarea en script:
"start": "concurrently \"npm run watch:scss\" \"npm run dev \""
ejecución:
npm run start

instalar copyfile
npm install --save-dev copyfiles

instalar imagemin:
npm install -g imagemin-cli --unsafe-perm-true -allow-root

tareas
"clean": "rimraf dist",
"imagemin": "imagemin imagenes/* --out-dir dist/imagenes"

ejecutar imagemin:
npm run imagemin

intalar usemin:
npm install --save-dev usemin-cli cssmin uglifyjs htmlmin

tarea usemin:
"usemin": "usemin index.html -d dist --htmlmin -o dist/index.html && usemin about.html -d dist --htmlmin -o dist/about.html y si sucesivamente... 

ejecutar:
npm run usemin

tarea build:
"build" : "npm run clean && npm run scss && npm run imagemin && npm run usemin"

ejecutar:
npm run build